<?php

namespace App\Controllers;

use App\Services\Routes;
use App\Tools\Statistics;
use App\Tools\User;

class Auth
{
    private static $acceptAuthUrl = [
        'login',
        'register',
        'auth/login',
        'auth/register',
    ];

    public static function getAcceptUrl(){
        return self::$acceptAuthUrl;
    }

    public static function register(){
        if(!empty($_POST)){
            $userId = User::addUser($_POST);
            if($userId > 1){
                Statistics::addEvent('register', $userId);
            }

            Routes::redirect('/login');
        }
    }

    public static function login() {
        if(!empty($_POST)){

            $user = User::getUser($_POST);
            if(!empty($user)){
                Statistics::addEvent('login', $user->id);

                $_SESSION['user'] = [
                    'name' => $user->name,
                    'userId' => $user->id,
                    'groupId' => $user->group_id,
                ];

                Routes::redirect('/');
            }

        }
    }

    public static function logout() {
        Statistics::addEvent('logout', $_SESSION['user']['userId']);
        unset($_SESSION['user']);
        Routes::redirect('/login');
    }
}