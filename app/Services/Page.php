<?php

namespace App\Services;

class Page
{
    public static function includeArea($path) {
        require_once 'views/pages/'. $path .'.php';
    }
}