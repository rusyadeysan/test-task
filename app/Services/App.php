<?php

namespace App\Services;

class App
{
    public static function init(){
        self::libs();
        self::db();
    }

    private static function db()
    {
        try {
            $db = require_once 'config/db.php';
            \R::setup( 'mysql:host='.$db['host'].';port='.$db['port'].';dbname='.$db['database'], $db['username'], $db['password']);
            \R::testConnection();
        } catch (\Exception $exception) {
            die('Error connection');
        }
    }

    private static function libs()
    {
        $config = require_once 'config/app.php';
        foreach ($config['libs'] as $lib){
            require_once "libs/" . $lib . '.php';
        }
    }
}