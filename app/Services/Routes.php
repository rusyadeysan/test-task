<?php

namespace App\Services;

use App\Controllers\Auth;
use App\Tools\User;

class Routes
{

    private static $list = [];
    public static function page($url, $page) {
        self::$list[] = [
            'url' => $url,
            'page' => $page,
        ];
    }

    public static function index() {
        $query = $_GET['q'];
        $showPage404 = true;

        if(!User::isAuth() && !in_array($query, Auth::getAcceptUrl())){
            Routes::redirect('/login');
        }

        foreach (self::$list as $route){
            if($route['url'] === '/' . $query){
                if($route['action'] === true){
                    $action = new $route['class'];
                    $method = $route['method'];
                    $action->$method();
                } else {
                    require_once 'views/pages/' . $route['page'] . '.php';
                }
                $showPage404 = false;
            }
        }

        if($showPage404) {
            Page::includeArea('main/404');
        }
    }

    public static function action($url, $class, $method) {
        self::$list[] = [
            'url' => $url,
            'class' => $class,
            'method' => $method,
            'action' => true,
        ];
    }

    public static function redirect($url) {
        header('Location: ' . $url);
        exit;
    }
}