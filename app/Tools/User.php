<?php

namespace App\Tools;
class User
{
    protected static $tableName = 'users';

    public static function getTableName()
    {
        return self::$tableName;
    }
    public static function addUser($data){
        $user = \R::dispense(self::$tableName);
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = $data['password'];
        $user->group_id = 2;

        return \R::store($user);
    }

    public static function getUser ($data){
        $email = $data['email'];
        $password = $data['password'];

        $user = \R::findOne(self::$tableName, 'email=?', [$email]);

        if(!empty($user) && $user->password == $password){
            return $user;
        }
    }

    public static function isAuth(){
        $auth = false;
        if(!empty($_SESSION['user'])){
            $auth = true;
        }

        return $auth;
    }

    public static function isAdmin(){
        $admin = false;
        if(!empty($_SESSION['user']) && $_SESSION['user']['groupId'] == 1){
            $admin = true;
        }

        return $admin;
    }
}