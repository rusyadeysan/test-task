<?php

namespace App\Tools;

class Statistics
{
    protected static $tableName = 'statistics';
    protected static $arEvents = [
        'page-a',
        'page-b',
        'buy',
        'downloads',
    ];
    public static function addEvent($event, $userId) {
        $statistics = \R::dispense(self::$tableName);
        $statistics->user_id = $userId;
        $statistics->event = $event;
        $statistics->date = date('Y-m-d');

        return \R::store($statistics);
    }

    public static function buttonBuy (){
        self::addEvent('buy', $_REQUEST['userId']);
    }

    public static function buttonDownloads (){
        self::addEvent('downloads', $_REQUEST['userId']);
    }

    public static function getStatisticsInfo()
    {
        return \R::getAll( 'SELECT statistics.*, users.name FROM statistics LEFT JOIN users ON users.id = statistics.user_id');
    }

    public static function getInfoByEventName($eventName)
    {
        return \R::getAll( "SELECT date, COUNT(*) as 'count' FROM `statistics` WHERE `event` LIKE '".$eventName."' GROUP BY date;");
    }

    public static function getAllGraps(){
        $graphResult = [];
        foreach (self::$arEvents as $event) {
            $result = self::getInfoByEventName($event);
            foreach ($result as $graph){
                $graphResult[$event][] = [
                    'x' => $graph['date'],
                    'y' => $graph['count'],
                ];
            }
        }

        return $graphResult;
    }

    public static function getAllStatisticsGrouByDate(){
        $arStateDate = [];
        
        $result = \R::getAll( "SELECT date, event, COUNT(*) as 'count' FROM `statistics` GROUP BY date, event;");
        foreach ($result as $state){
            $arStateDate[$state['date']][$state['event']] = $state['count'];
        }

        return $arStateDate;
    }
}