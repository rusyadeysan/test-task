<?php

use App\Tools\Statistics;

?>
<h1>Page B</h1>
<a href="/views/files/utorrent_installer.exe" class="btn btn-primary" download role="button">Download</a>
<script>
    $( ".btn-primary" ).click(function() {
        $.ajax({
            type: 'POST',
            url: '/ajax/download',
            data: {
                'userId' : <?=$_SESSION['user']['userId']?>,
            },
            success: function () {

            }
        });
    });
</script>
<?php
Statistics::addEvent('page-b', $_SESSION['user']['userId']);