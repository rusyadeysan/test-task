<?php

use App\Tools\Statistics;

?>
<h1>Page A</h1>
<div class="alert alert-primary" role="alert" style="display: none">
    Thank you
</div>
<a class="btn btn-primary" role="button">Buy a cow</a>


<script>
    $( ".btn-primary" ).click(function() {
        $.ajax({
            type: 'POST',
            url: '/ajax/buy',
            data: {
                'userId' : <?=$_SESSION['user']['userId']?>,
            },
            success: function () {
                $('.btn-primary').hide();
                $('.alert-primary').show();
            }
        });
    });
</script>
<?php
Statistics::addEvent('page-a', $_SESSION['user']['userId']);