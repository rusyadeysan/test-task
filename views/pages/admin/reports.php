<?php
use App\Tools\Statistics;

$allGraphs = Statistics::getAllGraps();
$allStatistics = Statistics::getAllStatisticsGrouByDate();
?>
<h1>Reports</h1>
<div class="row">
    <?php foreach ($allGraphs as $event => $graph){?>
        <div class="col-sm-6 text-center">
            <label class="label label-success"><?=$event?></label>
            <div id="<?=$event?>"></div>
        </div>
        <script>
            var data = <?=json_encode($graph)?>,
                config = {
                    data: data,
                    xkey: 'x',
                    ykeys: 'y',
                    labels: ['Total Income'],
                    fillOpacity: 0.6,
                    hideHover: 'auto',
                    behaveLikeLine: true,
                    resize: true,
                    pointFillColors:['#ffffff'],
                    pointStrokeColors: ['black'],
                    lineColors:['green']
                };
            config.element = '<?=$event?>';
            Morris.Area(config);
        </script>
    <?php }?>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Date</th>
            <th scope="col">Page view A</th>
            <th scope="col">Page view B</th>
            <th scope="col">click Buy a cow</th>
            <th scope="col">click Download</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($allStatistics as $date => $state){?>
            <tr>
                <td><?=$date?></td>
                <td><?=$state['page-a']?></td>
                <td><?=$state['page-b']?></td>
                <td><?=$state['buy']?></td>
                <td><?=$state['downloads']?></td>
            </tr>
        <?php }?>
        </tbody>
    </table>
</div>
<style>
    #page-a,
    #page-b,
    #buy,
    #downloads{
        min-height: 250px;
    }
</style>