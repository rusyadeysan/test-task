<?

use App\Tools\Statistics;

$statisticsInfo = Statistics::getStatisticsInfo();
?>
<h1>Statistic</h1>

<p>A table with third party integration  extension Filter control extension Data export</a> pour exporter</p>
<?php if(!empty($statisticsInfo)){?>
    <div id="toolbar">
        <select class="form-control">
            <option value="">Export Basic</option>
            <option value="all">Export All</option>
            <option value="selected">Export Selected</option>
        </select>
    </div>

    <table id="table"
           data-toggle="table"
           data-search="true"
           data-filter-control="true"
           data-show-export="true"
           data-click-to-select="true"
           data-toolbar="#toolbar"
           class="table-responsive">
        <thead>
        <tr>
            <th data-field="state" data-checkbox="true"></th>
            <th data-field="prenom" data-filter-control="input" data-sortable="true">Name</th>
            <th data-field="date" data-filter-control="select" data-sortable="true">Date</th>
            <th data-field="event" data-filter-control="select" data-sortable="true">Event</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($statisticsInfo as $index => $statistic){?>
            <tr>
                <td class="bs-checkbox ">
                    <input data-index="<?=$index?>" name="btSelectItem" type="checkbox">
                </td>
                <td><?=$statistic['name']?></td>
                <td><?=$statistic['date']?></td>
                <td><?=$statistic['event']?></td>
            </tr>
        <?php }?>
        </tbody>
    </table>
    <script>
        //exporte les données sélectionnées
        var $table = $('#table');
        $(function () {
            $('#toolbar').find('select').change(function () {
                $table.bootstrapTable('refreshOptions', {
                    exportDataType: $(this).val()
                });
            });
        })

        var trBoldBlue = $("table");

        $(trBoldBlue).on("click", "tr", function (){
            $(this).toggleClass("bold-blue");
        });
    </script>
<?php } ?>