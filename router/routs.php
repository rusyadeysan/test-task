<?php

use App\Services\Page;
use App\Services\Routes;
use App\Tools\Statistics;
use App\Controllers\Auth;

//page
Routes::page('/', 'main/home');
Routes::page('/login', 'authentication/login');
Routes::page('/register', 'authentication/register');
Routes::page('/page-a', 'page-a');
Routes::page('/page-b', 'page-b');
Routes::page('/statistic', 'admin/statistic');
Routes::page('/reports', 'admin/reports');

//action
Routes::action('/auth/register', Auth::class, 'register');
Routes::action('/auth/login', Auth::class, 'login');
Routes::action('/auth/logout', Auth::class, 'logout');
Routes::action('/ajax/buy', Statistics::class, 'buttonBuy');
Routes::action('/ajax/download', Statistics::class, 'buttonDownloads');

Page::includeArea('main/header');
Routes::index();
Page::includeArea('main/footer');